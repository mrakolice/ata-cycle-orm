<?php


namespace Ata\Cycle\Tests;

use Orchestra\Testbench\TestCase;
use Spiral\Database\Driver\Postgres\PostgresDriver;

abstract class BaseTestCase extends TestCase
{

    protected function getEnvironmentSetUp($app)
    {
        $cycleConfig = include __DIR__ . '/../config/cycle.php';

        $app['config']->set('cycle', $cycleConfig);
        $app['config']->set('cycle.schema.path', '/app/tests/Models');

        $app['config']->set('cycle.migrations.directory', '/app/tests/cycle_migrations');

        $app['config']->set('cycle.database.databases.default.connection', 'postgres');

        $app['config']->set('cycle.database.connections.postgres.driver', PostgresDriver::class);
        $app['config']->set('cycle.database.connections.postgres.options.connection', 'pgsql:host=pgsql;dbname=pg-data');
        $app['config']->set('cycle.database.connections.postgres.options.username', 'pg-user');
        $app['config']->set('cycle.database.connections.postgres.options.password', 'pg-secret');
    }



    protected function setUp(): void
    {
        parent::setUp();;
        $this->artisan("cycle:migrate");
        $this->artisan("cycle:migrate up");
    }

    protected function tearDown(): void
    {
        $this->artisan("cycle:migrate down");

        array_map('unlink', glob('/app/tests/cycle_migrations/*.php'));

        parent::tearDown();
    }

    protected function getPackageProviders($app)
    {
        return ['Ata\Cycle\PackageServiceProvider'];
    }
}
