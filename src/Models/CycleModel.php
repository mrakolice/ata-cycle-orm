<?php


namespace Ata\Cycle\Models;

use Cycle\Annotated\Annotation\Column;
use Cycle\ORM\Select\Repository;
use Cycle\ORM\Transaction;

trait CycleModel
{
    protected $attributes = [];

    /**  @Column(type="primary") */
    public $id;

    public static function orm(): Repository
    {
        return resolve('cycle-db')->getRepository(get_called_class());
    }

    public function __construct(array $data = [])
    {
        $this->__setData($data);
    }

    public function __setData(array $data)
    {
        if (count($data) == 0){
            return;
        }
        $this->attributes = $data;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function __getData(): array
    {
        return $this->attributes;
    }

    public function __get($name)
    {
        return $this->attributes[$name];
    }

    public function __set($name, $value)
    {
        if ($name == 'related'){
            dump($value);
        }

        $this->attributes[$name] = $value;
    }

    public function save($mode = Transaction::MODE_CASCADE)
    {
        $transaction = resolve('cycle-db.transaction');
        $transaction->persist($this, $mode);
        $transaction->run();

        return $this;
    }

    public function delete()
    {
        $transaction = resolve('cycle-db.transaction');
        $transaction->delete($this);
        $transaction->run();
    }
}
