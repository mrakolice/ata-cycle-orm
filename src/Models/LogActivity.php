<?php


namespace Ata\Cycle\Models;


use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Table\Index;


/**
 * @Entity
 * @Table(columns = {
 *          "log_name"      : @Column(type="string", nullable=true),
 *          "description"   : @Column(type="text"),
 *          "subject_id"    : @Column(type="bigInteger", nullable=true),
 *          "subject_type"  : @Column(type="string", nullable=true),
 *          "causer_id"     : @Column(type="bigInteger", nullable=true),
 *          "causer_type"   : @Column(type="string", nullable=true),
 *          "properties"    : @Column(type="json", nullable=true),
 *     }, indexes = {
 *          @Index(columns={"log_name"}),
 *          @Index(columns={"subject_id", "subject_type"}, name="subject"),
 *          @Index(columns={"causer_id", "causer_type"}, name="causer"),
 *     })
 */
class LogActivity
{
    use CycleModel;

    public static function logEntity($entity, $action): ?LogActivity
    {
        if (!config('cycle.log_activity.use')) {
            return null;
        }

        return (new LogActivity([
            'log_name' => 'models_tracking',
            'description' => $action,
            'subject_id' => $entity->id,
            'subject_type' => $entity->class,
            'properties' => $entity->attributes
        ]))->save();
    }


    public static function logEntityChange(object $entity, array $changedData): ?LogActivity
    {
        if (!config('cycle.log_activity.use')) {
            return null;
        }

        return (new LogActivity([
            'log_name' => 'models_tracking',
            'description' => config('cycle.log_activity.events.updated'),
            'subject_id' => $entity->id,
            'subject_type' => $entity->class,
            'properties' => $changedData
        ]))->save();
    }


}
