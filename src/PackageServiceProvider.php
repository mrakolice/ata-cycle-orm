<?php

namespace Ata\Cycle;

use Ata\Cycle\Console\Commands\CycleMigrate;
use Cycle\Migrations\GenerateMigrations;
use Cycle\ORM\Factory;
use Cycle\ORM\Transaction;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Illuminate\Support\ServiceProvider;
use Spiral\Database\Config\DatabaseConfig;
use Spiral\Database\DatabaseManager;
use Spiral\Migrations\Config\MigrationConfig;
use Spiral\Migrations\FileRepository;
use Spiral\Migrations\Migrator;
use Cycle\Schema;
use Cycle\Annotated;
use Cycle\ORM;


class PackageServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMigrator();

        $this->registerCycleDb();
    }

    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/cycle.php', 'cycle'
        );

        if ($this->app->runningInConsole()) {
            $this->commands([
                CycleMigrate::class
            ]);
        }
    }

    protected function registerMigrator()
    {
        $this->app->singleton('cycle-db.migrator.config', function ($app) {
            return new MigrationConfig(config('cycle.migrations'));
        });

        $this->app->singleton('cycle-db.migrator', function ($app) {
            $migrator = new Migrator($app['cycle-db.migrator.config'], $app['cycle-db.db-manager'], new FileRepository($app['cycle-db.migrator.config']));

            // Init migration table
            $migrator->configure();

            return $migrator;
        });

        $this->app->singleton(Migrator::class, 'cycle-db.migrator');
    }

    protected function registerCycleDb()
    {
        // The connection factory is used to create the actual connection instances on
        // the database. We will inject the factory into the manager so that it may
        // make the connections while they are actually needed and not of before.
        $this->app->singleton('cycle-db.db-manager', function ($app) {
            return new DatabaseManager(
                new DatabaseConfig(config('cycle.database'))
            );
        });

        // The database manager is used to resolve various connections, since multiple
        // connections might be managed. It also implements the connection resolver
        // interface which may be used by other components requiring connections.
        $this->app->singleton('cycle-db.db-factory', function ($app) {

            $modelsPath = config('cycle.schema.path');

            if (config('cycle.log_activity.use')) {
                array_merge($modelsPath, [__DIR__ . '/Models']);
            }

            $finder = (new \Symfony\Component\Finder\Finder())->files()->in($modelsPath);
            $classLocator = new \Spiral\Tokenizer\ClassLocator($finder);

            // autoload annotations
            AnnotationRegistry::registerLoader('class_exists');

            $schema = (new Schema\Compiler())->compile(new Schema\Registry($app['cycle-db.db-manager']),
                array_merge([
                    new Annotated\Entities($classLocator),              // register annotated entities
                    new Annotated\Embeddings($classLocator),            // register embeddable entities
                ], config('cycle.schema.generators'),
                    [
                        new GenerateMigrations($app['cycle-db.migrator']->getRepository(), $app['cycle-db.migrator.config']),
                    ])
            );
            return (new ORM\ORM(new Factory($app['cycle-db.db-manager'])))->withSchema(new ORM\Schema($schema));
        });

        $this->app->bind('cycle-db', function ($app) {
            return $app['cycle-db.db-factory']->withHeap(new ORM\Heap\Heap());
        });

        $this->app->bind('cycle-db.transaction', function ($app) {
            return new Transaction($app['cycle-db']);
        });
    }
}
