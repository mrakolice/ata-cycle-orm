<?php


namespace Ata\Cycle\Mappers;

use Ata\Cycle\Models\CycleModel;
use Ata\Cycle\Models\LogActivity;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Table;
use Cycle\ORM\Command\Database\Update;
use Cycle\ORM\Context\ConsumerInterface;
use Cycle\ORM\Heap\Node;
use Cycle\ORM\Heap\State;
use Cycle\ORM\Mapper\Mapper;
use Cycle\ORM\Command\ContextCarrierInterface;
use Cycle\ORM\Command\CommandInterface;
use Cycle\ORM\Schema;

/**
 * @Table(
 *      columns={
 *          "created_at": @Column(type="datetime"),
 *          "updated_at": @Column(type="datetime"),
 *          "deleted_at": @Column(type="datetime", nullable=true),
 *      },
 * )
 */
class DefaultMapper extends Mapper
{
    public function init(array $data): array
    {
        $class = $this->orm->getSchema()->define($this->role, Schema::ENTITY);

        // return empty entity and prepared data
        return [new $class, $data];
    }

    public function extract($entity): array
    {
        /** @var CycleModel $entity */
        return array_merge(
            $entity->__getData(),
            parent::extract($entity)
        );
    }

    public function hydrate($entity, array $data)
    {
        $entity = parent::hydrate($entity, $data);
        $entity->__setData($data);
        return $entity;
    }


    protected function fetchFields($entity): array
    {
        // ignore properties which are not declated in schema
        return array_intersect_key(
            $this->extract($entity),
            $this->columns
        );
    }

    public function queueCreate($entity, Node $node, State $state): ContextCarrierInterface
    {
        $cmd = parent::queueCreate($entity, $node, $state);

        $state->register('created_at', new \DateTimeImmutable(), true);
        $cmd->register('created_at', new \DateTimeImmutable(), true);

        $state->register('updated_at', new \DateTimeImmutable(), true);
        $cmd->register('updated_at', new \DateTimeImmutable(), true);

        LogActivity::logEntity($entity, config('cycle.log_activity.events.created'));

        return $cmd;
    }

    public function queueUpdate($entity, Node $node, State $state): ContextCarrierInterface
    {
        $data = $this->fetchFields($entity);
        LogActivity::logEntityChange($entity, array_udiff_assoc($data, $state->getData(), [static::class, 'compare']));

        /** @var Update $cmd */
        $cmd = parent::queueUpdate($entity, $node, $state);

        $state->register('updated_at', new \DateTimeImmutable(), true);
        $cmd->registerAppendix('updated_at', new \DateTimeImmutable());


        return $cmd;
    }

    public function queueDelete($entity, Node $node, State $state): CommandInterface
    {

        LogActivity::logEntity($entity, config('cycle.log_activity.events.deleted'));

        if ($state->getStatus() == Node::SCHEDULED_DELETE) {
            return parent::queueDelete($entity, $node, $state);
        }

        // identify entity as being "deleted"
        $state->setStatus(Node::SCHEDULED_DELETE);
        $state->decClaim();

        $cmd = new Update(
            $this->source->getDatabase(),
            $this->source->getTable(),
            ['deleted_at' => new \DateTimeImmutable()]
        );

        // forward primaryKey value from entity state
        // this sequence is only required if the entity is created and deleted
        // within one transaction
        $cmd->waitScope($this->primaryColumn);
        $state->forward(
            $this->primaryKey,
            $cmd,
            $this->primaryColumn,
            true,
            ConsumerInterface::SCOPE
        );

        return $cmd;
    }

}
